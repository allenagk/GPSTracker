package com.android.gpstracker.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.gpstracker.Model.Post;
import com.android.gpstracker.R;
import com.android.gpstracker.Util;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import static com.android.gpstracker.AppConstants.POSTS_COLLECTION_REF;
import static com.android.gpstracker.AppConstants.POST_IMAGES_STORAGE_REF;

public class AddPorductActivity extends AppCompatActivity implements View.OnClickListener {

    private View parentLayout;
    private EditText etTitle;
    private EditText etPrice;
    private EditText etDesc;
    private ImageButton imgBtnPhotoUrl1;
    private Button submit;
    private ProgressDialog mProgress;
    private Uri mImageUri1 = null;
    private static final int GALLERY_REQUEST_1 = 1;

    private static final String TAG = "AddProductActivity";

    private FirebaseAuth mAuth;
    private FirebaseStorage mStorage;
    private FirebaseFirestore mDatabase;
    private Post newPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_porduct);

        mAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance();
        mDatabase = FirebaseFirestore.getInstance();

        initViews();
    }

    private void initViews() {
        parentLayout = findViewById(android.R.id.content);
        etTitle = findViewById(R.id.et_title);
        etPrice = findViewById(R.id.et_price);
        etDesc = findViewById(R.id.et_desc);
        imgBtnPhotoUrl1 = findViewById(R.id.imgbtn_photo1);

        submit = findViewById(R.id.btn_submit);
        mProgress = new ProgressDialog(this);
        mProgress.setMessage(getString(R.string.please_wait));

        submit.setOnClickListener(this);
        imgBtnPhotoUrl1.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                setupSubmit();
                break;
            case R.id.imgbtn_photo1:
                Intent galleryIntent1 = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent1.setType("image/*");
                startActivityForResult(galleryIntent1, GALLERY_REQUEST_1);
                break;
        }
    }

    private void setupSubmit() {
        String title = etTitle.getText().toString().trim();
        String price = etPrice.getText().toString().trim();
        String desc = etDesc.getText().toString().trim();

        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(price)) {

            if (mImageUri1 != null) {

                newPost = new Post(title, price, mAuth.getUid());
                newPost.setLive(true);
                newPost.setDesc(desc);

                uploadPhoto(mImageUri1);

            } else {
                Snackbar.make(parentLayout, "You must upload a photo", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

        } else {
            Snackbar.make(parentLayout, "Please fill all the required fields", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    private void uploadPhoto(Uri image_uri) {
        mProgress.setMessage(getString(R.string.please_wait));
        mProgress.show();

        StorageReference filePath = mStorage.getReference()
                .child(POST_IMAGES_STORAGE_REF).child(Util.randomUUID());

        filePath.putFile(image_uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                mProgress.dismiss();
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                newPost.setPhoto_url1(downloadUrl.toString());

                //Adding details to database with photoUrl
                writeDocument(newPost);
                Toast.makeText(AddPorductActivity.this, "Product has been posted", Toast.LENGTH_LONG).show();
                finish();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgress.dismiss();
                //Toast.makeText(AddPostActivity.this, "Something went wrong",
                //      Toast.LENGTH_SHORT).show();
                Snackbar.make(parentLayout, "Something went wrong, Try again later...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void writeDocument(Post post) {
        mDatabase.collection(POSTS_COLLECTION_REF).add(post);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST_1 && resultCode == RESULT_OK) {
            mImageUri1 = data.getData();
            imgBtnPhotoUrl1.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imgBtnPhotoUrl1.setImageURI(mImageUri1);

        }
    }
}
