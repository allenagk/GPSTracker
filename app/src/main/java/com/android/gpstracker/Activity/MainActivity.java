package com.android.gpstracker.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.gpstracker.Adapter.PostViewHolder;
import com.android.gpstracker.AppConstants;
import com.android.gpstracker.Model.Post;
import com.android.gpstracker.Model.User;
import com.android.gpstracker.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;

import static com.android.gpstracker.AppConstants.FASTEST_INTERVAL;
import static com.android.gpstracker.AppConstants.MY_PERMISSIONS_REQUEST_LOCATION;
import static com.android.gpstracker.AppConstants.POSTS_COLLECTION_REF;
import static com.android.gpstracker.AppConstants.POST_DOC_ID_BUNDLE_KEY;
import static com.android.gpstracker.AppConstants.UPDATE_INTERVAL;
import static com.android.gpstracker.AppConstants.USERS_COLLECTION_REF;
import static com.android.gpstracker.AppConstants.USER_ID_BUNDLE_KEY;
import static com.android.gpstracker.Util.getLocalPrice;
import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawerLayout;

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private final static String TAG = "MainActivity";

    TextView tvFullName, tvEmail;
    User muser;
    NavigationView mnavigationView;
    private FirestoreRecyclerAdapter mAdapter;
    private RecyclerView mPostList;
    private ProgressDialog mProgressDialog;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationCallback locationCallback;
    private boolean locationPermission = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ActionBar actionbar = getSupportActionBar();

        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);


        mDrawerLayout = findViewById(R.id.drawer_layout);

        mnavigationView = findViewById(R.id.nav_view);
        View header = mnavigationView.getHeaderView(0);
        tvFullName = header.findViewById(R.id.nav_header_textView);
        tvEmail = header.findViewById(R.id.nav_header_textEmail);

        checkLocationPermission();
        fetchUserData();

        mnavigationView.setNavigationItemSelectedListener(this);


        mPostList = findViewById(R.id.post_list);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mPostList.addItemDecoration(new DividerItemDecoration(mPostList.getContext(),
                LinearLayoutManager.VERTICAL));
        mPostList.setLayoutManager(linearLayoutManager);
        mPostList.setItemAnimator(new DefaultItemAnimator());

        getPostList();

    }

    private void getPostList() {

        Query query = db.collection(POSTS_COLLECTION_REF);

        FirestoreRecyclerOptions<Post> response = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Post, PostViewHolder>(response) {

            @Override
            protected void onBindViewHolder(PostViewHolder holder, int position, Post model) {
                holder.setTitle(model.getTitle());
                holder.setDesc(model.getDesc());
                holder.setPrice(getLocalPrice(model.getPrice()));
                holder.setImageView(model.getPhoto_url1());

                final String docId = getSnapshots().getSnapshot(position).getId();
                Log.d("DOC ID: ", docId);
                Log.d("RESULT TITLE: ", model.getTitle());
                //Log.d("RESULT DATE: ", model.getDate().toString());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO: ProductActivity
                        Intent intent = new Intent(MainActivity.this, ProductActivity.class);
                        intent.putExtra(POST_DOC_ID_BUNDLE_KEY, docId);
                        startActivity(intent);
                        Log.d(TAG, docId.toString());
                    }
                });
            }

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_item, parent, false);

                return new PostViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

        };

        mAdapter.startListening();
        mAdapter.notifyDataSetChanged();
        mPostList.setAdapter(mAdapter);

    }

    private void fetchUserData() {
        Log.d(TAG, mAuth.getUid());
        tvEmail.setText(mAuth.getCurrentUser().getEmail());

        DocumentReference docRef = db.collection(USERS_COLLECTION_REF).document(mAuth.getUid());

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                mProgressDialog.hide();
                if (documentSnapshot.exists()) {
                    muser = documentSnapshot.toObject(User.class);
                    if (muser != null) {
                        Log.d(TAG, muser.getFirstName());
                        tvFullName.setText(muser.getFirstName() + " " + muser.getLastName());
                        tvEmail.setText(muser.getEmail());

                        Menu menuNav = mnavigationView.getMenu();
                        if (muser.isEmployee()) {
                            menuNav.findItem(R.id.nav_add).setEnabled(true);
                            menuNav.findItem(R.id.nav_manage_users).setEnabled(true);
                            menuNav.findItem(R.id.nav_manage_products).setEnabled(true);
                            menuNav.findItem(R.id.nav_gallery).setEnabled(false);
                        } else {
                            menuNav.findItem(R.id.nav_gallery).setEnabled(true);
                        }

                    }
                } else {
                    Toast.makeText(MainActivity.this,
                            R.string.contact_service_provider, Toast.LENGTH_SHORT).show();
                    signOut();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgressDialog.hide();
                Toast.makeText(MainActivity.this,
                        R.string.contact_service_provider, Toast.LENGTH_SHORT).show();
                signOut();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_search:
                Intent intent = new Intent(this, SearchActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            //item.setChecked(true);
        } else if (id == R.id.nav_about) {
            view_About();
        } else if (id == R.id.nav_gallery) {
            Intent intent = new Intent(MainActivity.this, OrdersActivity.class);
            intent.putExtra(USER_ID_BUNDLE_KEY, mAuth.getUid());
            startActivity(intent);
        } else if (id == R.id.nav_add) {
            startActivity(new Intent(this, AddPorductActivity.class));
        } else if (id == R.id.nav_logout) {
            showLogoutDialog(this);
        } else if (id == R.id.nav_account) {
            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            intent.putExtra(USER_ID_BUNDLE_KEY, mAuth.getUid());
            startActivity(intent);
        } else if (id == R.id.nav_manage_users) {
            Intent intent = new Intent(MainActivity.this, ManageUserActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_manage_products) {
            Intent intent = new Intent(MainActivity.this, ManageProductActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showLogoutDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure?");
        //builder.setTitle("Delete".toUpperCase());
        builder.setPositiveButton("Yes"
                , new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        signOut();
                    }
                });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void signOut() {
        mAuth.signOut();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void view_About() {
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater factory = LayoutInflater.from(MainActivity.this);
        final View textEntryview = factory.inflate(R.layout.about, null);
        alt_bld.setView(textEntryview).setCancelable(true)
                .setNegativeButton(getResources().getString(R.string.about_negative_button)
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = alt_bld.create();
        alert.setTitle(getResources().getString(R.string.about_title));
        alert.setIcon(R.mipmap.ic_launcher);
        alert.show();
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            createGoggleApiClients();
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        } else {
            locationPermission = true;
            createGoggleApiClients();
        }
    }

    //creating FusedLocationClient
    public void createGoggleApiClients() {
        fusedLocationProviderClient = getFusedLocationProviderClient(this);
    }

    //start the device location updates
    protected void startLocationUpdates() {
        System.out.println("////startloacation");

        LocationRequest mLocationRequest = LocationRequest.create()
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                onLocationChanged(locationResult.getLastLocation());
            }
        };

        fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, locationCallback, null);
    }

    //location changes will call back onLocationChanged
    public void onLocationChanged(Location location) {

        System.out.println("////locations-->"+location);

        // New location has now been determined
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        GeoPoint geoPoint = new GeoPoint(location.getLatitude(), location.getLongitude());
        if (muser != null) {
            muser.setGeoPoint(geoPoint);
            updateDriverLocation(muser);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(locationPermission) {
            System.out.println("////////method called");
            if (mAuth.getCurrentUser().getEmail().equalsIgnoreCase(AppConstants.DRIVER_EMAIL)) {
                startLocationUpdates();
            }
        }
    }

    // user given Permissions will be handled here
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        locationPermission = true;
                    }
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))

                        Toast.makeText(this, "no permission", Toast.LENGTH_SHORT).show();
                    locationPermission = false;
                }
                return;
            }
        }
    }

    protected void onStop() {
        super.onStop();
        if (locationPermission) {
            if(fusedLocationProviderClient != null && locationCallback != null) {
                fusedLocationProviderClient.removeLocationUpdates(locationCallback);
            }
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (locationPermission) {
            if(fusedLocationProviderClient != null && locationCallback != null) {
                fusedLocationProviderClient.removeLocationUpdates(locationCallback);
            }
        }
    }

    private void updateDriverLocation(User mUser) {
        //mProgressDialog.setMessage("Please wait...");
        //mProgressDialog.show();
        db.collection(USERS_COLLECTION_REF).document(mAuth.getUid()).set(mUser).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //mProgressDialog.hide();
                Toast.makeText(MainActivity.this, "Location Update: Success",
                        Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //mProgressDialog.hide();
                Toast.makeText(MainActivity.this, "Something went wrong with location update",
                        Toast.LENGTH_LONG).show();
            }
        });
    }
}