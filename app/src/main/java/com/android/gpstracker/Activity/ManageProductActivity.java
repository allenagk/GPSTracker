package com.android.gpstracker.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.gpstracker.Adapter.PostViewHolder;
import com.android.gpstracker.Model.Post;
import com.android.gpstracker.Model.User;
import com.android.gpstracker.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import static com.android.gpstracker.AppConstants.ADMIN_BUNDLE_KEY;
import static com.android.gpstracker.AppConstants.POSTS_COLLECTION_REF;
import static com.android.gpstracker.AppConstants.POST_DOC_ID_BUNDLE_KEY;
import static com.android.gpstracker.Util.getLocalPrice;

/**
 * Created by savantis-allen on 10/28/18.
 */
public class ManageProductActivity extends AppCompatActivity {

    private final static String TAG = "ManageProductActivity";

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    User muser;
    private FirestoreRecyclerAdapter mAdapter;
    private RecyclerView mPostList;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);


        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        mPostList = findViewById(R.id.post_list);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mPostList.addItemDecoration(new DividerItemDecoration(mPostList.getContext(),
                LinearLayoutManager.VERTICAL));
        mPostList.setLayoutManager(linearLayoutManager);
        mPostList.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    protected void onResume() {
        super.onResume();

        getPostList();
    }

    private void getPostList() {

        Query query = db.collection(POSTS_COLLECTION_REF);

        FirestoreRecyclerOptions<Post> response = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Post, PostViewHolder>(response) {

            @Override
            protected void onBindViewHolder(PostViewHolder holder, int position, Post model) {
                holder.setTitle(model.getTitle());
                holder.setDesc(model.getDesc());
                /*holder.setStart_bid(getLocalPrice(model.getPrice()));
                holder.setCurrent_bid(getLocalPrice(model.getCurrent_bid()));
                holder.setStart_date(model.getDate().toString());
                holder.setEnd_date(model.getEnd_date().toString());
                holder.setBids(model.getBids());*/

                holder.setImageView(model.getPhoto_url1());

                final String docId = getSnapshots().getSnapshot(position).getId();
                Log.d("DOC ID: ", docId);
                Log.d("RESULT TITLE: ", model.getTitle());
                //Log.d("RESULT DATE: ", model.getDate().toString());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ManageProductActivity.this, ProductActivity.class);
                        intent.putExtra(POST_DOC_ID_BUNDLE_KEY, docId);
                        intent.putExtra(ADMIN_BUNDLE_KEY, true);
                        startActivity(intent);
                        Log.d(TAG, docId.toString());
                    }
                });
            }

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_item, parent, false);

                return new PostViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

        };

        mAdapter.startListening();
        mAdapter.notifyDataSetChanged();
        mPostList.setAdapter(mAdapter);

    }
}
