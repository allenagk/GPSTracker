package com.android.gpstracker.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.gpstracker.Adapter.UserViewHolder;
import com.android.gpstracker.Model.User;
import com.android.gpstracker.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import static com.android.gpstracker.AppConstants.ADMIN_BUNDLE_KEY;
import static com.android.gpstracker.AppConstants.USERS_COLLECTION_REF;
import static com.android.gpstracker.AppConstants.USER_ID_BUNDLE_KEY;


/**
 * Created by savantis-allen on 10/28/18.
 */
public class ManageUserActivity extends AppCompatActivity {

    private final static String TAG = "ManageUserActivity";
    private FirebaseFirestore db;

    private FirebaseAuth mAuth;
    private FirestoreRecyclerAdapter mAdapter;
    private RecyclerView mUsertList;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);


        mUsertList = findViewById(R.id.post_list);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mUsertList.addItemDecoration(new DividerItemDecoration(mUsertList.getContext(),
                LinearLayoutManager.VERTICAL));
        mUsertList.setLayoutManager(linearLayoutManager);
        mUsertList.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    protected void onResume() {
        super.onResume();

        getUsersList();
    }

    private void getUsersList() {

        Query query = db.collection(USERS_COLLECTION_REF);

        FirestoreRecyclerOptions<User> response = new FirestoreRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<User, UserViewHolder>(response) {

            @Override
            protected void onBindViewHolder(UserViewHolder holder, int position, User model) {
                holder.setTitle(model.getFirstName() + " " + model.getLastName());
                holder.setDesc(model.getEmail());

                final String docId = getSnapshots().getSnapshot(position).getId();

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ManageUserActivity.this, RegisterActivity.class);
                        intent.putExtra(USER_ID_BUNDLE_KEY, docId);
                        intent.putExtra(ADMIN_BUNDLE_KEY, true);
                        startActivity(intent);
                        Log.d(TAG, docId.toString());
                    }
                });
            }

            @Override
            public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.user_list_item, parent, false);

                return new UserViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

        };

        mAdapter.startListening();
        mAdapter.notifyDataSetChanged();
        mUsertList.setAdapter(mAdapter);

    }
}
