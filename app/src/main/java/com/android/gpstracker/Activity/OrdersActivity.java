package com.android.gpstracker.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.gpstracker.Adapter.OrderViewHolder;
import com.android.gpstracker.AppConstants;
import com.android.gpstracker.Map.MapsActivity;
import com.android.gpstracker.Model.Order;
import com.android.gpstracker.Model.User;
import com.android.gpstracker.R;
import com.android.gpstracker.Util;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;

import static com.android.gpstracker.AppConstants.ORDERS_COLLECTION_REF;
import static com.android.gpstracker.AppConstants.ORDER_DOC_ID_BUNDLE_KEY;
import static com.android.gpstracker.AppConstants.PARCELABLE_LAT_REF;
import static com.android.gpstracker.AppConstants.PARCELABLE_LONG_REF;
import static com.android.gpstracker.AppConstants.USERS_COLLECTION_REF;
import static com.android.gpstracker.AppConstants.USER_ID_BUNDLE_KEY;

public class OrdersActivity extends AppCompatActivity {

    private static final String TAG = "OrdersActivity";
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirestoreRecyclerAdapter mAdapter;
    private RecyclerView mOrderList;
    private ProgressDialog mProgressDialog;

    private String userid;
    private GeoPoint location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();


        mOrderList = findViewById(R.id.orders_list);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mOrderList.addItemDecoration(new DividerItemDecoration(mOrderList.getContext(),
                LinearLayoutManager.VERTICAL));
        mOrderList.setLayoutManager(linearLayoutManager);
        mOrderList.setItemAnimator(new DefaultItemAnimator());

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (getIntent().hasExtra(USER_ID_BUNDLE_KEY)) {
                userid = extras.getString(USER_ID_BUNDLE_KEY);
            }
            /*if (getIntent().hasExtra(MY_BIDS_BUNDLE_KEY)) {
                isMybids = getIntent().getBooleanExtra(MY_BIDS_BUNDLE_KEY, false);
            }

            if (getIntent().hasExtra(MY_AUCTIONS_BUNDLE_KEY)) {
                isMyAuctions = getIntent().getBooleanExtra(MY_AUCTIONS_BUNDLE_KEY, false);
            }*/
        } else {
            Log.d(TAG, "No Extras");
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //fetch location data and then fetch orderlist
        fetchLocationData();
    }

    public void getOrderList() {

        Query query = db.collection(ORDERS_COLLECTION_REF).whereEqualTo("user_id", userid);

        FirestoreRecyclerOptions<Order> response = new FirestoreRecyclerOptions.Builder<Order>()
                .setQuery(query, Order.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Order, OrderViewHolder>(response) {

            @Override
            protected void onBindViewHolder(OrderViewHolder holder, int position, final Order model) {
                holder.setTitle(model.getPost_ordered().getTitle());
                holder.setOrder_date(model.getOrdered_date().toString());
                holder.setState(Util.getState(model.getState()));
                holder.setImageView(model.getPost_ordered().getPhoto_url1());

                final String docId = getSnapshots().getSnapshot(position).getId();

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        AlertDialog.Builder builderSingle = new AlertDialog.Builder(OrdersActivity.this);
                        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(OrdersActivity.this, android.R.layout.select_dialog_singlechoice);
                        arrayAdapter.add("View Item Details");
                        arrayAdapter.add("View Current Location");

                        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String strName = arrayAdapter.getItem(which);
                                switch (which) {
                                    case 0:
                                        Intent intent = new Intent(OrdersActivity.this, ProductActivity.class);
                                        intent.putExtra(ORDER_DOC_ID_BUNDLE_KEY, docId);
                                        startActivity(intent);
                                        break;
                                    case 1:
                                        Log.d("LatLng is : ", model.getGeo_point().getLatitude()
                                                + " " + model.getGeo_point().getLongitude());
                                        if (location != null) {
                                            Intent i = new Intent(OrdersActivity.this, MapsActivity.class);
                                            i.putExtra(PARCELABLE_LAT_REF, location.getLatitude());
                                            i.putExtra(PARCELABLE_LONG_REF, location.getLongitude());
                                            startActivity(i);
                                        } else {
                                            Toast.makeText(OrdersActivity.this,
                                                    "No google location set for this product", Toast.LENGTH_SHORT).show();
                                        }
                                        break;
                                    case 2:
                                        break;
                                    default:
                                        break;
                                }

                            }
                        });
                        builderSingle.show();
                    }
                });
            }

            @Override
            public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.order_list_item, parent, false);

                return new OrderViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

        };

        mAdapter.startListening();
        mAdapter.notifyDataSetChanged();
        mOrderList.setAdapter(mAdapter);
    }

    private void fetchLocationData() {
        Log.d(TAG, mAuth.getUid());
        //tvEmail.setText(mAuth.getCurrentUser().getEmail());

        DocumentReference docRef = db.collection(USERS_COLLECTION_REF).document(AppConstants.DRIVER_USER_ID);

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    User muser = documentSnapshot.toObject(User.class);
                    if (muser != null) {
                        location = muser.getGeoPoint();
                        getOrderList();
                    }
                } else {
                    Toast.makeText(OrdersActivity.this,
                            R.string.contact_service_provider, Toast.LENGTH_SHORT).show();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(OrdersActivity.this,
                        R.string.contact_service_provider, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
