package com.android.gpstracker.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.gpstracker.Map.MapsActivity;
import com.android.gpstracker.Model.Order;
import com.android.gpstracker.Model.Post;
import com.android.gpstracker.R;
import com.android.gpstracker.Util;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.squareup.picasso.Picasso;

import static com.android.gpstracker.AppConstants.ADMIN_BUNDLE_KEY;
import static com.android.gpstracker.AppConstants.FROM_AUCTIONS_BUNDLE_KEY;
import static com.android.gpstracker.AppConstants.FROM_MY_BIDS_BUNDLE_KEY;
import static com.android.gpstracker.AppConstants.ORDERS_COLLECTION_REF;
import static com.android.gpstracker.AppConstants.ORDER_DOC_ID_BUNDLE_KEY;
import static com.android.gpstracker.AppConstants.PARCELABLE_ADDRESS_REF;
import static com.android.gpstracker.AppConstants.PARCELABLE_LAT_REF;
import static com.android.gpstracker.AppConstants.PARCELABLE_LONG_REF;
import static com.android.gpstracker.AppConstants.POSTS_COLLECTION_REF;
import static com.android.gpstracker.AppConstants.POST_DOC_ID_BUNDLE_KEY;

public class ProductActivity extends AppCompatActivity {

    private String postDocID;

    private static final String TAG = "ProductActivity";
    private FirebaseFirestore db;

    private TextView tvTitle;
    private TextView tvDeliveryState;
    private TextView tvPrice;
    private TextView tvDesc;
    private ImageView ivPoster;
    private EditText etQuantity;
    private EditText etAddress;

    private ScrollView scrollView;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;
    private ProgressDialog mProgress;

    Post post = null;
    Order order = null;
    double latitude = 0, longitude = 0;
    String address = null;
    private boolean isAdmin;
    private boolean from_mybids;
    private boolean from_auctions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        //Initialize the ui by findViewById
        initView();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (getIntent().hasExtra(POST_DOC_ID_BUNDLE_KEY)) {
                postDocID = extras.getString(POST_DOC_ID_BUNDLE_KEY);
                Log.d("POST DOC ID: ", postDocID);
                getPost(postDocID);
                isAdmin = extras.getBoolean(ADMIN_BUNDLE_KEY, false);
                if (isAdmin) {
                    setupAdminUI();
                }
            } else if (getIntent().hasExtra(ORDER_DOC_ID_BUNDLE_KEY)) {
                postDocID = extras.getString(ORDER_DOC_ID_BUNDLE_KEY);
                Log.d("ORDER DOC ID: ", postDocID);
                getOrder(postDocID);
            }
            if (getIntent().hasExtra(FROM_MY_BIDS_BUNDLE_KEY)) {
                from_mybids = getIntent().getBooleanExtra(FROM_MY_BIDS_BUNDLE_KEY, false);
            }
            if (getIntent().hasExtra(FROM_AUCTIONS_BUNDLE_KEY)) {
                from_auctions = getIntent().getBooleanExtra(FROM_AUCTIONS_BUNDLE_KEY, false);
            }
        } else {
            finish();
        }
    }

    private void setupAdminUI() {
        findViewById(R.id.btn_order).setVisibility(View.GONE);
        findViewById(R.id.btn_delete_product).setVisibility(View.VISIBLE);
    }

    //Initialize the ui by findviewbyid
    private void initView() {

        mProgress = new ProgressDialog(this);
        mProgress.setMessage(getString(R.string.please_wait));
        scrollView = findViewById(R.id.scrollView);
        progressBar = findViewById(R.id.progressBar);
        ivPoster = findViewById(R.id.image_view_big);
        etAddress = findViewById(R.id.et_address);
        //etAddress.setText(address);

        tvTitle = findViewById(R.id.tv_pd_title);
        tvPrice = findViewById(R.id.tv_pd_price);
        tvDeliveryState = findViewById(R.id.tv_pd_delivery_state);
        tvDesc = findViewById(R.id.tv_pd_desc);

        etQuantity = findViewById(R.id.et_qnty);
    }

    //Set page status load or loaded
    private void setLoading(boolean loading) {
        if (loading) {
            scrollView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            scrollView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

    //Send request to get post details, if success set the textViews, else show error
    private void getPost(String doc_id) {
        setLoading(true);
        DocumentReference docRef = db.collection(POSTS_COLLECTION_REF).document(doc_id);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    setLoading(false);
                    post = documentSnapshot.toObject(Post.class);
                    Log.d(TAG, "Post Title" + " => " + post.getTitle());
                    try {
                        setViews(post);
                    } catch (Exception e) {
                        Toast.makeText(ProductActivity.this,
                                getString(R.string.toast_went_wrong), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(ProductActivity.this,
                            getString(R.string.toast_went_wrong), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    //Send request to get post details, if success set the textViews, else show error
    private void getOrder(String doc_id) {
        setLoading(true);
        DocumentReference docRef = db.collection(ORDERS_COLLECTION_REF).document(doc_id);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    setLoading(false);
                    order = documentSnapshot.toObject(Order.class);
                    Log.d(TAG, "Order UserID" + " => " + order.getUser_id());
                    try {
                        setViews(order);
                    } catch (Exception e) {
                        Toast.makeText(ProductActivity.this,
                                getString(R.string.toast_went_wrong), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(ProductActivity.this,
                            getString(R.string.toast_went_wrong), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    private void setViews(Post post) throws Exception {

        //Set image poster
        Picasso.get()
                .load(post.getPhoto_url1())//.resize(50, 50)
                //.centerCrop()
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .error(R.drawable.ic_menu_info)
                .into(ivPoster);

        tvTitle.setText(post.getTitle());
        tvPrice.setText(Util.getLocalPrice(post.getPrice()));
        tvDesc.setText(post.getDesc());
    }

    private void setViews(Order order) throws Exception {
        post = order.getPost_ordered();

        findViewById(R.id.crd_order_detail).setVisibility(View.GONE);
        //Set image poster
        Picasso.get()
                .load(post.getPhoto_url1())//.resize(50, 50)
                //.centerCrop()
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .error(R.drawable.ic_menu_info)
                .into(ivPoster);

        tvTitle.setText(post.getTitle());
        tvPrice.setText(Util.getLocalPrice(post.getPrice()));
        tvDesc.setText(post.getDesc());
    }

    public void onPickLocation(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivityForResult(intent, 1);
    }

    public void onPlaceOrder(View view) {

        if (!etQuantity.getText().toString().isEmpty() && !etAddress.getText().toString().isEmpty()) {

            int quantity = Integer.parseInt(etQuantity.getText().toString());
            //GeoPoint geoPoint = new GeoPoint(latitude, longitude);

            if (post != null) {

                mProgress.show();
                Order new_order = new Order(mAuth.getCurrentUser().getUid(),
                        post, quantity, Order.d_states.FRESH.getState());
                new_order.setGeo_address(etAddress.getText().toString());

                db.collection(ORDERS_COLLECTION_REF).document().set(new_order)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                mProgress.dismiss();
                                Toast.makeText(ProductActivity.this,
                                        getString(R.string.toast_order_placed), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                mProgress.dismiss();
                                Toast.makeText(ProductActivity.this,
                                        getString(R.string.toast_went_wrong), Toast.LENGTH_SHORT).show();
                            }
                        });
            }

        } else {
            Log.d(TAG, String.valueOf(R.string.toast_order_details_empty));
            Toast.makeText(ProductActivity.this,
                    getString(R.string.toast_order_details_empty), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {

                address = data.getStringExtra(PARCELABLE_ADDRESS_REF);
                etAddress.setText(address);

                latitude = data.getDoubleExtra(PARCELABLE_LAT_REF, 5);
                longitude = data.getDoubleExtra(PARCELABLE_LONG_REF, 10);

                Log.d(TAG, "Address: " + address + " Lat: " + latitude + " Longitude: " + longitude);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    //Delete current product from database
    public void onDeleteProduct(View view) {
        mProgress.show();
        db.collection(POSTS_COLLECTION_REF).document(postDocID)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mProgress.hide();
                        Toast.makeText(ProductActivity.this, "Successfully deleted the Auction product",
                                Toast.LENGTH_LONG).show();
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mProgress.hide();
                        Toast.makeText(ProductActivity.this, "Something went wrong, Please try later",
                                Toast.LENGTH_LONG).show();
                    }
                });
    }
}
