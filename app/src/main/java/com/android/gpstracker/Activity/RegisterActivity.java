package com.android.gpstracker.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.gpstracker.Model.User;
import com.android.gpstracker.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import static com.android.gpstracker.AppConstants.ADMIN_BUNDLE_KEY;
import static com.android.gpstracker.AppConstants.USERS_COLLECTION_REF;
import static com.android.gpstracker.AppConstants.USER_ID_BUNDLE_KEY;

public class RegisterActivity extends Activity {

    private FirebaseAuth mAuth;
    FirebaseFirestore mDb;

    private ProgressDialog mProgressDialog;

    private final static String TAG = "RegisterActivity";

    private EditText etFirstName, etLastName, etAddress, etPhone, etEmail, etPassword;
    //private RadioButton rbCustomer, rbEmployee;

    private User mUser;
    private boolean isAdmin;
    private String userid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        mDb = FirebaseFirestore.getInstance();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        initViews();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (getIntent().hasExtra(USER_ID_BUNDLE_KEY)) {
                userid = extras.getString(USER_ID_BUNDLE_KEY);
                getUserData(userid);
                findViewById(R.id.btn_register).setVisibility(View.GONE);
                findViewById(R.id.btn_update).setVisibility(View.VISIBLE);
            }

            if (getIntent().getBooleanExtra(ADMIN_BUNDLE_KEY, false)) {
                setupAdminUI();
            }
        }

    }

    private void setupAdminUI() {
        findViewById(R.id.btn_register).setVisibility(View.GONE);
        findViewById(R.id.btn_update).setVisibility(View.GONE);
        findViewById(R.id.btn_delete_user).setVisibility(View.VISIBLE);

        etFirstName.setEnabled(false);
        etLastName.setEnabled(false);
        etAddress.setEnabled(false);
        etPhone.setEnabled(false);
        etEmail.setEnabled(false);
        etEmail.setEnabled(false);
        etPassword.setEnabled(false);
        //spinner.setEnabled(false);
    }

    private void initViews() {
        etFirstName = findViewById(R.id.et_firstname);
        etLastName = findViewById(R.id.et_lastname);
        etAddress = findViewById(R.id.et_address);
        etPhone = findViewById(R.id.et_phone);
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        //rbCustomer = findViewById(R.id.rb_customer);
        //rbEmployee = findViewById(R.id.rb_employee);
    }

    private void getUserData(String userid) {
        mProgressDialog.show();

        //tvEmail.setText(mAuth.getCurrentUser().getEmail());

        DocumentReference docRef = mDb.collection(USERS_COLLECTION_REF).document(userid);

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                mProgressDialog.hide();
                if (documentSnapshot.exists()) {
                    mUser = documentSnapshot.toObject(User.class);
                    if (mUser != null) {
                        Log.d(TAG, mUser.getFirstName());
                        etFirstName.setText(mUser.getFirstName());
                        etLastName.setText(mUser.getLastName());
                        etAddress.setText(mUser.getAddress());
                        etPhone.setText(mUser.getPhone());
                        etEmail.setText(mUser.getEmail());
                        etEmail.setEnabled(false);
                        etPassword.setEnabled(false);
                    }
                } else {
                    finish();
                }

            }
        });
    }

    private boolean isValidated() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String address = etAddress.getText().toString();
        String phone = etPhone.getText().toString();
        String email = etEmail.getText().toString();

        boolean isEmployee = false;

        if (!firstName.isEmpty() && !address.isEmpty() && !phone.isEmpty()) {

            /*if (rbEmployee.isChecked()) {
                isEmployee = true;
            }
            if (rbCustomer.isChecked()) {
                isEmployee = false;
            }*/

            mUser = new User(firstName, address, phone, isEmployee, email);

            if (!lastName.isEmpty()) {
                mUser.setLastName(lastName);
            }

            return true;
        } else {
            return false;
        }
    }


    private void registerNewUser(String email, String password, final User user) {
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.show();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mProgressDialog.hide();
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            //FirebaseUser user = mAuth.getCurrentUser();
                            String uid = mAuth.getCurrentUser().getUid();

                            //Save user object in database
                            mDb.collection(USERS_COLLECTION_REF).document(uid).set(user);

                            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgressDialog.hide();
            }
        });
    }

    public void onRegister(View view) {

        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        if (!email.isEmpty() && !password.isEmpty()) {
            if (isValidated()) {
                registerNewUser(email, password, mUser);
            } else {
                Toast.makeText(RegisterActivity.this, "Please fill the required fields",
                        Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(RegisterActivity.this, "Please enter valid email & password",
                    Toast.LENGTH_LONG).show();
        }

    }

    public void onUpdate(View view) {
        if (isValidated()) {
            updateUser(mUser);
        } else {
            Toast.makeText(RegisterActivity.this, "Please fill the required fields",
                    Toast.LENGTH_LONG).show();
        }
    }

    private void updateUser(User mUser) {
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.show();
        mDb.collection(USERS_COLLECTION_REF).document(mAuth.getUid()).set(mUser).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mProgressDialog.hide();
                Toast.makeText(RegisterActivity.this, "Success",
                        Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgressDialog.hide();
                Toast.makeText(RegisterActivity.this, "Something went wrong, Please try later",
                        Toast.LENGTH_LONG).show();
            }
        });

    }

    public void onDelete(View view) {
        mProgressDialog.show();
        mDb.collection(USERS_COLLECTION_REF).document(userid)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mProgressDialog.hide();
                        Toast.makeText(RegisterActivity.this, "Successfully deleted the user",
                                Toast.LENGTH_LONG).show();
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mProgressDialog.hide();
                        Toast.makeText(RegisterActivity.this, "Something went wrong, Please try later",
                                Toast.LENGTH_LONG).show();
                    }
                });
    }
}
