package com.android.gpstracker.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.gpstracker.R;
import com.squareup.picasso.Picasso;

public class OrderViewHolder extends RecyclerView.ViewHolder {

    View mView;

    public OrderViewHolder(View itemView) {
        super(itemView);

        mView = itemView;
    }

    public void setTitle(String title) {
        TextView post_title = mView.findViewById(R.id.tv_item_name);
        post_title.setText(title);
    }

    public void setOrder_date(String order_date) {
        TextView post_order = mView.findViewById(R.id.tv_ordered_date);
        post_order.setText(order_date);
    }

    public void setState(String state) {
        TextView post_state = mView.findViewById(R.id.tv_state);
        post_state.setText(state);
    }


    public void setImageView(String url) {
        ImageView imagePost = mView.findViewById(R.id.img_product);
        Picasso.get()
                .load(url)//.resize(50, 50)
                //.centerCrop()
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .error(R.drawable.ic_menu_info)
                .into(imagePost);
    }
}
