package com.android.gpstracker.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.gpstracker.R;


/**
 * Created by savantis-allen on 10/28/18.
 */
public class UserViewHolder extends RecyclerView.ViewHolder {


    View mView;

    public UserViewHolder(View itemView) {
        super(itemView);

        mView = itemView;
    }

    public void setTitle(String title) {
        TextView user_title = mView.findViewById(R.id.tv_user_name);
        user_title.setText(title);
    }

    public void setDesc(String desc) {
        TextView user_desc = mView.findViewById(R.id.tv_user_email);
        user_desc.setText(desc);
    }

}
