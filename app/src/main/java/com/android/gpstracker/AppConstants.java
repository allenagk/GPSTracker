package com.android.gpstracker;

public class AppConstants {

    //Firebase Database References
    public static final String USERS_COLLECTION_REF = "users";
    public static final String POSTS_COLLECTION_REF = "posts";
    public static final String ORDERS_COLLECTION_REF = "orders";

    //Firebase Storage References
    public static final String POST_IMAGES_STORAGE_REF = "post_images";

    //Bundle keys
    public static final String POST_DOC_ID_BUNDLE_KEY = "post_key";
    public static final String ORDER_DOC_ID_BUNDLE_KEY = "order_key";
    public static final String USER_ID_BUNDLE_KEY = "user_id_key";
    public static final String ADMIN_BUNDLE_KEY = "admin";
    public static final String FROM_AUCTIONS_BUNDLE_KEY = "from_my_auctions_key";
    public static final String FROM_MY_BIDS_BUNDLE_KEY = "from_my_bids_key";

    //Parcelable data between productActivity and mapActivity
    public static final String PARCELABLE_LAT_REF = "latitude";
    public static final String PARCELABLE_LONG_REF = "longitude";
    public static final String PARCELABLE_ADDRESS_REF = "address";

    //Location Update MainActivity Constants
    public static final long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    public static final long FASTEST_INTERVAL = 2000; /* 2 sec */
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 100;

    public static final String DRIVER_EMAIL = "driver@gmail.com";
    public static final String DRIVER_USER_ID = "V4Ql7dL4cSeS3DXacLCtZv6t6RG3";
}
