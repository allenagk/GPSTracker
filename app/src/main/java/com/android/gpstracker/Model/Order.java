package com.android.gpstracker.Model;

import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.sql.Timestamp;
import java.util.Date;

@IgnoreExtraProperties
public class Order {

    private int quantity;
    private int state;
    private GeoPoint geo_point;
    private Date ordered_date;
    private Post post_ordered;
    private String user_id;
    private String geo_address;

    public Order() {
    }

    public Order(String user_id, Post post_ordered, int quantity, int state) {
        this.user_id = user_id;
        this.post_ordered = post_ordered;
        this.quantity = quantity;
        //this.geo_point = geo_point;
        this.state = state;
        this.ordered_date = new Timestamp(System.currentTimeMillis());
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public GeoPoint getGeo_point() {
        return geo_point;
    }

    public void setGeo_point(GeoPoint geo_point) {
        this.geo_point = geo_point;
    }

    public Date getOrdered_date() {
        return ordered_date;
    }

    public void setOrdered_date(Date ordered_date) {
        this.ordered_date = ordered_date;
    }

    public Post getPost_ordered() {
        return post_ordered;
    }

    public void setPost_ordered(Post post_ordered) {
        this.post_ordered = post_ordered;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGeo_address() {
        return geo_address;
    }

    public void setGeo_address(String geo_address) {
        this.geo_address = geo_address;
    }

    public enum d_states {
        FRESH(0),
        PROCESSING(1),
        ONTHEWAY(2),
        COMPLETED(3);

        private int state;

        d_states(int state) {
            this.state = state;
        }

        public int getState() {
            return state;
        }
    }
}
