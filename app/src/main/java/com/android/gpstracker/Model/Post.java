package com.android.gpstracker.Model;

import com.google.firebase.firestore.IgnoreExtraProperties;

import java.sql.Timestamp;
import java.util.Date;


@IgnoreExtraProperties
public class Post {

    //private String uid;
    private String title;
    private String desc;
    private String price;
    private Date date;
    private String photo_url1;
    private String employee_id;
    private boolean live;


    public Post() {
    }

    //All required fields only
    public Post(String title, String price, String employee_id) {
        //this.uid = uid;
        this.title = title;
        this.price = price;
        //this.photo_url1 = url;
        this.employee_id = employee_id;
        this.date = new Timestamp(System.currentTimeMillis());
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getPhoto_url1() {
        return photo_url1;
    }

    public void setPhoto_url1(String photo_url1) {
        this.photo_url1 = photo_url1;
    }

}
