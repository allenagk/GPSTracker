package com.android.gpstracker.Model;

import com.google.firebase.firestore.GeoPoint;

public class User {

    private String firstName;
    private String lastName;
    private boolean isEmployee;
    private String address;
    private String phone;
    private String email;
    private GeoPoint geoPoint;

    public User() {
    }

    public User(String firstName, String address, String phone, boolean isEmployee, String email) {
        this.firstName = firstName;
        this.address = address;
        this.phone = phone;
        this.isEmployee = isEmployee;
        this.email = email;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isEmployee() {
        return isEmployee;
    }

    public void setEmployee(boolean employee) {
        isEmployee = employee;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
