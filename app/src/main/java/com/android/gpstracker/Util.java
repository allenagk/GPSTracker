package com.android.gpstracker;

import android.net.Uri;

import com.android.gpstracker.Model.Order;

import java.util.UUID;

public class Util {
    //Generate random String UUID
    public static String randomUUID() {
        String randomUUID = UUID.randomUUID().toString();
        return randomUUID;
    }

    public static String getImageNameFromUri(Uri uri) {
        String url = uri.toString();
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        return fileName;
    }

    public static String getLocalPrice(String price) {
        String stringPrice = "Rs. " + price;
        return stringPrice;
    }

    public static String getState(int state) {
        if (Order.d_states.FRESH.getState() == state) {
            return Order.d_states.FRESH.name();
        } else if (Order.d_states.PROCESSING.getState() == state) {
            return Order.d_states.FRESH.name();
        } else if (Order.d_states.ONTHEWAY.getState() == state) {
            return Order.d_states.FRESH.name();
        } else if (Order.d_states.COMPLETED.getState() == state) {
            return Order.d_states.FRESH.name();
        } else {
            return "Undefined";
        }
    }
}
