package com.android.gpstracker;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        System.out.println("Output : "+solution(1041));     //1 00000 1 000 1
    }

    public int solution(int N) {

        int gap = 0;
        String binary = Integer.toBinaryString(N);
        char[] chars = binary.toCharArray();

        List<Integer> positionof1s = new ArrayList<Integer>();

        for (int i=0;i<chars.length;i++){
            if(chars[i]=='1'){
                positionof1s.add(i);
            }
        }

       for (int k = 0; k<positionof1s.size();k++){
           System.out.println(positionof1s.get(k));     //1 00000 1 000 1
       }

        System.out.println(positionof1s.size());     //1 00000 1 000 1


        if (positionof1s.size()<2){
            gap = 0;
        } else {
            //Iterator<Integer> myListIterator = positionof1s.iterator();
            //while (myListIterator.hasNext()) {
            int max = 0;
              for (int j=0;j<positionof1s.size()-1;j++){
                  int x = positionof1s.get(j+1)-positionof1s.get(j);
                  System.out.println("x ; "+x);     //1 00000 1 000 1
                  if (x>max){
                          max = x;
                      }
                  }
                  gap = max-1;
              }

        return gap;
    }

    @Test
    public void substraction_isCorrect() {

        int[] A = {3, 8, 9, 7, 6};      //[6, 3, 8, 9, 7]
        int K = 3;

        int[] y = recursiveFunc(A, 3);

        for(int i=0;i<y.length;i++) {
            System.out.print(y[i]+" ");
        }

        }

        public int[] recursiveFunc(int[] A, int k){
            int[] y;
            if(k>0){
                y = shiftedArray(A);
                k--;
                return recursiveFunc(y, k);
            } else {
                return A;
            }
        }

    public int[] shiftedArray(int[] A){

        int length = A.length;
        int[] x = new int[length];

        for(int i=1;i<length;i++){
                x[i] = A[i-1];
        }
        x[0] = A[length-1];

        return x;
    }
}